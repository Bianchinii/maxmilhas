//
//  Constants.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit


struct Colors {
    static let blueThemeColor = #colorLiteral(red: 0.2124941945, green: 0.6128894687, blue: 0.7885354161, alpha: 1)
    static let greenThemeColor = #colorLiteral(red: 0.02608692087, green: 0.7744804025, blue: 0.6751230955, alpha: 1)
}

