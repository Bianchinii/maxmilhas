//
//  BaseViewController.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class BaseViewController: MMViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let navigationController = self.navigationController else { return }
        
        
        self.navigationItem.hidesBackButton = true
        navigationController.navigationBar.isTranslucent = false
                
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "",
                                                                style: .plain,
                                                                target: nil,
                                                                action: nil)
        
        if let navigationController = self.navigationController,
            navigationController.viewControllers.count > 1 {
            
            self.navigationItem.leftBarButtonItem = UIBarButtonItem.barButton(withImage: #imageLiteral(resourceName: "back_button"),
                                                                              target: self,
                                                                              action: #selector(goBack))
            
            weak var weakNav = self.navigationController
            self.navigationController?.interactivePopGestureRecognizer?.delegate = weakNav as? UIGestureRecognizerDelegate
        }
    }
    
    @IBAction func touchBackButton(_ sender: Any) {
        goBack()
    }
    
    @objc func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
