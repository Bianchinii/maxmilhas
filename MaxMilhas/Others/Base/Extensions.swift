//
//  Extensions.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    static func barButton(withImage image: UIImage,
                          target: Any?,
                          action: Selector?) -> UIBarButtonItem {
        
        let button = UIButton(type: .custom)
        button.isUserInteractionEnabled = true
        button.setImage(image, for: .normal)
        button.frame = CGRect(x: 0, y: 6, width: 44, height: 44)
        button.contentHorizontalAlignment = .left 
        
        if let target = target,
            let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        
        return UIBarButtonItem(customView: button)
    }
}


extension Date {
    
    static let kDefaultDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    static let kPortugueseBrazilianLocale = Locale(identifier: "pt_BR")
    static let kDateTimeFormat = "yyyy-MM-dd HH:mm:ss"
    static let kHourAndMinutes = "HH:mm"
    static let kDayMonthExtensionFormat = "dd MMM"
    
    static func converted(from date: String?, format: String = kDateTimeFormat) -> Date? {
        guard let date = date else {
            return Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = kPortugueseBrazilianLocale
        if let dateConverted = dateFormatter.date(from: date) {
            return dateConverted
        } else {
            return nil
        }
    }
    
    static func shortString(from date: Date, format: String = kDayMonthExtensionFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = kPortugueseBrazilianLocale
        let convertedDate = dateFormatter.string(from: date)
        return convertedDate
    }
    
    func hour(from date: Date) -> Int {
        let calendar = Calendar.current
        return calendar.component(.hour, from: date)
    }
}

extension Date {
    var time: Time {
        return Time(self)
    }
}
