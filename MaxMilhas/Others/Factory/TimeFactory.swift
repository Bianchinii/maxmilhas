//
//  TimeFactory.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class TimeFactory: NSObject {
    
    //MARK: Morning
    static func departureMorning() -> Time {
        return Time(06,00)
    }
    
    static func arrivalMorning() -> Time {
        return Time(11,59)
    }
    
    //MARK: Evening
    static func departureEvening() -> Time {
        return Time(12,00)
    }
    
    static func arrivalEvening() -> Time {
        return Time(17,59)
    }
    
    //MARK: Night
    static func departureNight() -> Time {
        return Time(18,00)
    }
    
    static func arrivalNight() -> Time {
        return Time(23,59)
    }
    
    //MARK: Dawn
    static func departureDawn() -> Time {
        return Time(00,00)
    }
    
    static func arrivalDawn() -> Time {
        return Time(05,59)
    }
}
