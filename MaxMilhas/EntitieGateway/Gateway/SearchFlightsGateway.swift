//
//  SearchFlightsGateway.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

typealias FetchFlightsCompletionHandler = (_ flight: (outbound: [MMFlight], inbound: [MMFlight])?, _ error: RequestError? ) -> Void

protocol SearchFlightsGateway {
    func fetchFlights(completionHandler: @escaping FetchFlightsCompletionHandler)

}

