//
//  MMFees.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMFees {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let value = "value"
    static let passengerType = "passengerType"
    static let type = "type"
    static let group = "group"
    static let id = "id"
  }

  // MARK: Properties
  public var value: Float?
  public var passengerType: String?
  public var type: String?
  public var group: String?
  public var id: Int?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    value = json[SerializationKeys.value].float
    passengerType = json[SerializationKeys.passengerType].string
    type = json[SerializationKeys.type].string
    group = json[SerializationKeys.group].string
    id = json[SerializationKeys.id].int
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = value { dictionary[SerializationKeys.value] = value }
    if let value = passengerType { dictionary[SerializationKeys.passengerType] = value }
    if let value = type { dictionary[SerializationKeys.type] = value }
    if let value = group { dictionary[SerializationKeys.group] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    return dictionary
  }

}
