//
//  MMFlight.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMFlight {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pricing = "pricing"
    static let choosedTripType = "choosedTripType"
    static let stops = "stops"
    static let cabin = "cabin"
    static let airlineTarget = "airlineTarget"
    static let to = "to"
    static let direction = "direction"
    static let otaAvailableIn = "otaAvailableIn"
    static let availableIn = "availableIn"
    static let id = "id"
    static let departureDate = "departureDate"
    static let airline = "airline"
    static let arrivalDate = "arrivalDate"
    static let from = "from"
    static let flightNumber = "flightNumber"
    static let duration = "duration"
    static let trips = "trips"
  }

  // MARK: Properties
  public var pricing: MMPricing?
  public var choosedTripType: String?
  public var stops: Int?
  public var cabin: String?
  public var airlineTarget: String?
  public var to: String?
  public var direction: String?
  public var otaAvailableIn: String?
  public var availableIn: String?
  public var id: String?
  public var departureDate: String?
  public var airline: String?
  public var arrivalDate: String?
  public var from: String?
  public var flightNumber: String?
  public var duration: Int?
  public var trips: [MMTrips]?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    pricing = MMPricing(json: json[SerializationKeys.pricing])
    choosedTripType = json[SerializationKeys.choosedTripType].string
    stops = json[SerializationKeys.stops].int
    cabin = json[SerializationKeys.cabin].string
    airlineTarget = json[SerializationKeys.airlineTarget].string
    to = json[SerializationKeys.to].string
    direction = json[SerializationKeys.direction].string
    otaAvailableIn = json[SerializationKeys.otaAvailableIn].string
    availableIn = json[SerializationKeys.availableIn].string
    id = json[SerializationKeys.id].string
    departureDate = json[SerializationKeys.departureDate].string
    airline = json[SerializationKeys.airline].string
    arrivalDate = json[SerializationKeys.arrivalDate].string
    from = json[SerializationKeys.from].string
    flightNumber = json[SerializationKeys.flightNumber].string
    duration = json[SerializationKeys.duration].int
    if let items = json[SerializationKeys.trips].array { trips = items.map { MMTrips(json: $0) } }
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = pricing { dictionary[SerializationKeys.pricing] = value.dictionaryRepresentation() }
    if let value = choosedTripType { dictionary[SerializationKeys.choosedTripType] = value }
    if let value = stops { dictionary[SerializationKeys.stops] = value }
    if let value = cabin { dictionary[SerializationKeys.cabin] = value }
    if let value = airlineTarget { dictionary[SerializationKeys.airlineTarget] = value }
    if let value = to { dictionary[SerializationKeys.to] = value }
    if let value = direction { dictionary[SerializationKeys.direction] = value }
    if let value = otaAvailableIn { dictionary[SerializationKeys.otaAvailableIn] = value }
    if let value = availableIn { dictionary[SerializationKeys.availableIn] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = departureDate { dictionary[SerializationKeys.departureDate] = value }
    if let value = airline { dictionary[SerializationKeys.airline] = value }
    if let value = arrivalDate { dictionary[SerializationKeys.arrivalDate] = value }
    if let value = from { dictionary[SerializationKeys.from] = value }
    if let value = flightNumber { dictionary[SerializationKeys.flightNumber] = value }
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    if let value = trips { dictionary[SerializationKeys.trips] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

}
