//
//  MMLuggage.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMLuggage {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let carryOn = "carryOn"
    static let checked = "checked"
  }

  // MARK: Properties
  public var carryOn: MMCarryOn?
  public var checked: MMChecked?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    carryOn = MMCarryOn(json: json[SerializationKeys.carryOn])
    checked = MMChecked(json: json[SerializationKeys.checked])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = carryOn { dictionary[SerializationKeys.carryOn] = value.dictionaryRepresentation() }
    if let value = checked { dictionary[SerializationKeys.checked] = value.dictionaryRepresentation() }
    return dictionary
  }

}
