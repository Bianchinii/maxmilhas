//
//  MMAdult.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMAdult {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fees = "fees"
    static let quantity = "quantity"
    static let fare = "fare"
  }

  // MARK: Properties
  public var fees: [MMFees]?
  public var quantity: Int?
  public var fare: Float?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    if let items = json[SerializationKeys.fees].array { fees = items.map { MMFees(json: $0) } }
    quantity = json[SerializationKeys.quantity].int
    fare = json[SerializationKeys.fare].float
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fees { dictionary[SerializationKeys.fees] = value.map { $0.dictionaryRepresentation() } }
    if let value = quantity { dictionary[SerializationKeys.quantity] = value }
    if let value = fare { dictionary[SerializationKeys.fare] = value }
    return dictionary
  }

}
