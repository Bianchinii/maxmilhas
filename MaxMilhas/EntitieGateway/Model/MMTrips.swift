//
//  MMTrips.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMTrips {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let layover = "layover"
    static let aircraft = "aircraft"
    static let departureDate = "departureDate"
    static let arrivalDate = "arrivalDate"
    static let from = "from"
    static let stops = "stops"
    static let isInternational = "isInternational"
    static let duration = "duration"
    static let flightNumber = "flightNumber"
    static let carrier = "carrier"
    static let to = "to"
  }

  // MARK: Properties
  public var layover: Int?
  public var aircraft: String?
  public var departureDate: String?
  public var arrivalDate: String?
  public var from: String?
  public var stops: Int?
  public var isInternational: Bool? = false
  public var duration: Int?
  public var flightNumber: String?
  public var carrier: String?
  public var to: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    layover = json[SerializationKeys.layover].int
    aircraft = json[SerializationKeys.aircraft].string
    departureDate = json[SerializationKeys.departureDate].string
    arrivalDate = json[SerializationKeys.arrivalDate].string
    from = json[SerializationKeys.from].string
    stops = json[SerializationKeys.stops].int
    isInternational = json[SerializationKeys.isInternational].boolValue
    duration = json[SerializationKeys.duration].int
    flightNumber = json[SerializationKeys.flightNumber].string
    carrier = json[SerializationKeys.carrier].string
    to = json[SerializationKeys.to].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = layover { dictionary[SerializationKeys.layover] = value }
    if let value = aircraft { dictionary[SerializationKeys.aircraft] = value }
    if let value = departureDate { dictionary[SerializationKeys.departureDate] = value }
    if let value = arrivalDate { dictionary[SerializationKeys.arrivalDate] = value }
    if let value = from { dictionary[SerializationKeys.from] = value }
    if let value = stops { dictionary[SerializationKeys.stops] = value }
    dictionary[SerializationKeys.isInternational] = isInternational
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    if let value = flightNumber { dictionary[SerializationKeys.flightNumber] = value }
    if let value = carrier { dictionary[SerializationKeys.carrier] = value }
    if let value = to { dictionary[SerializationKeys.to] = value }
    return dictionary
  }

}
