//
//  MMPricing.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMPricing {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let bestPriceAt = "bestPriceAt"
    static let airlineName = "airlineName"
    static let isInternational = "isInternational"
    static let mmBestPriceAt = "mmBestPriceAt"
    static let savingPercentage = "savingPercentage"
    static let ota = "ota"
  }

  // MARK: Properties
  public var bestPriceAt: String?
  public var airlineName: String?
  public var isInternational: Bool? = false
  public var mmBestPriceAt: String?
  public var savingPercentage: Int?
  public var ota: MMOta?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    bestPriceAt = json[SerializationKeys.bestPriceAt].string
    airlineName = json[SerializationKeys.airlineName].string
    isInternational = json[SerializationKeys.isInternational].boolValue
    mmBestPriceAt = json[SerializationKeys.mmBestPriceAt].string
    savingPercentage = json[SerializationKeys.savingPercentage].int
    ota = MMOta(json: json[SerializationKeys.ota])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = bestPriceAt { dictionary[SerializationKeys.bestPriceAt] = value }
    if let value = airlineName { dictionary[SerializationKeys.airlineName] = value }
    dictionary[SerializationKeys.isInternational] = isInternational
    if let value = mmBestPriceAt { dictionary[SerializationKeys.mmBestPriceAt] = value }
    if let value = savingPercentage { dictionary[SerializationKeys.savingPercentage] = value }
    if let value = ota { dictionary[SerializationKeys.ota] = value.dictionaryRepresentation() }
    return dictionary
  }

}
