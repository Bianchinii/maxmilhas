//
//  MMOta.swift
//
//  Created by Ítalo Bianchini on 20/02/19
//  Copyright (c) Ítalo Bianchini. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct MMOta {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fareTotal = "fareTotal"
    static let luggage = "luggage"
    static let familyCode = "familyCode"
    static let saleTotal = "saleTotal"
    static let checkout = "checkout"
    static let adult = "adult"
  }

  // MARK: Properties
  public var fareTotal: Float?
  public var luggage: MMLuggage?
  public var familyCode: String?
  public var saleTotal: Float?
  public var checkout: MMCheckout?
  public var adult: MMAdult?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public init(json: JSON) {
    fareTotal = json[SerializationKeys.fareTotal].float
    luggage = MMLuggage(json: json[SerializationKeys.luggage])
    familyCode = json[SerializationKeys.familyCode].string
    saleTotal = json[SerializationKeys.saleTotal].float
    checkout = MMCheckout(json: json[SerializationKeys.checkout])
    adult = MMAdult(json: json[SerializationKeys.adult])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fareTotal { dictionary[SerializationKeys.fareTotal] = value }
    if let value = luggage { dictionary[SerializationKeys.luggage] = value.dictionaryRepresentation() }
    if let value = familyCode { dictionary[SerializationKeys.familyCode] = value }
    if let value = saleTotal { dictionary[SerializationKeys.saleTotal] = value }
    if let value = checkout { dictionary[SerializationKeys.checkout] = value.dictionaryRepresentation() }
    if let value = adult { dictionary[SerializationKeys.adult] = value.dictionaryRepresentation() }
    return dictionary
  }

}
