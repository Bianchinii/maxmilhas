//
//  SearchFlightsAPI.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SearchFlightsAPI: APIManager, SearchFlightsGateway {
    private let baseUrl = "https://vcugj6hmt5.execute-api.us-east-1.amazonaws.com/hmg-search"
    
    func fetchFlights(completionHandler: @escaping FetchFlightsCompletionHandler) {
        
        Alamofire.request(baseUrl,
                          method: .get,
                          parameters: nil,
                          encoding: JSONEncoding.default, headers: headerDefault())
            
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                switch response.result {
                case .success(let value):
                   

                    let json = JSON(value)
                    let jsonListOutbound = json["outbound"].array
                    let jsonListInbound = json["inbound"].array
                    
                
                    var outboundList = [MMFlight]()
                    for flight in jsonListOutbound! {
                        outboundList.append(MMFlight(json:flight as JSON))
                    }
                    
                    var inboundList = [MMFlight]()
                    for flight in jsonListInbound! {
                        inboundList.append(MMFlight(json:flight as JSON))
                    }
                    
                    completionHandler(( outboundList,inboundList) , self.requestError(response: response.response))
                case .failure( _):
                    completionHandler(nil,self.requestError(response: response.response))
                }
        }
    }
}
