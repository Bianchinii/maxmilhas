//
//  APIManager.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

enum RequestError :Int{
    case invalidCredentials = 401
    case notFound = 404
    case dataConflict = 409
    case sucess = 200
    case connectionFailure = 1009
    case unprocessableEntity = 422
    case failure = 500
}

class APIManager: NSObject {
    
    
    //MARK: Header
    public func headerDefault() ->Dictionary<String,String> {
        return ["Content-Type":"application/json"]
    }
    
    
    //MARK: EndPoint
    public func searchFlightsRouter() -> String {
        return "https://developer.goibibo.com/docs/api/search/"
    }
    
    
    func requestError(response : HTTPURLResponse?) -> RequestError{
        if response != nil {
            
            let code : Int = (response?.statusCode)!
            switch code {
            case 200 ... 299: return .sucess
            case 401: return .invalidCredentials
            case 404: return .notFound
            case 402 ... 422: return .unprocessableEntity
            case 500: return .failure
            default: return .failure
            }
            
        }else{
            return RequestError.connectionFailure
        }
    }


}
