//
//  SearchFlightsCoordinator.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class SearchFlightsCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    var storyboardIdentifier = "Flights"
    var flightPresenter: FlightPresenter?
    
    init(navigationController: UINavigationController, delegate: CoordinatorDelegate?) {
        self.navigationController = navigationController
        self.coordinatorDelegate = delegate
    }
    
    func start() {
        showSearchViewController()
    }
    
    // MARK: - ShowControllers
    
    private func showSearchViewController() {
        let controller = MMSearchFlightsViewController.initFromStoryboard(named: storyboardIdentifier)
        controller.delegate = self
        controller.coordinator = self
        self.navigationController.setViewControllers([controller], animated: true)
    }
    
    private func showSortViewController() {
        let controller = MMSortViewController.initFromStoryboard(named: storyboardIdentifier)
        controller.delegate = self
        self.navigationController.pushViewController(controller, animated: true)
    }
    
    private func showFilterViewController(data: FlightDataSurvey, filter: [FlightFilter]) {
        let controller = FilterFlightViewController.initFromStoryboard(named: storyboardIdentifier)
        controller.delegate = self
        controller.flightDataSurvey = data
        controller.filterList = filter
        self.navigationController.pushViewController(controller, animated: true)
    }
}


extension SearchFlightsCoordinator: MMSearchFlightsViewControllerDelegate {
    func didTapFilter(data: FlightDataSurvey) {
        showFilterViewController(data: data, filter: flightPresenter?.getFlightFilterList() ?? [])
    }

    func didTapSort() {
        showSortViewController()
    }
}


extension SearchFlightsCoordinator: MMSortViewControllerDelegate {
    func didTapToApply() {
        
    }
}

extension SearchFlightsCoordinator: FilterFlightViewControllerDelegate {
    func didTapCleanButton() {
        flightPresenter?.applyFilter(flightFilter: [])
        self.navigationController.popViewController(animated: true)
    }
    
    func didTapToApplyFilter(flightFilter: [FlightFilter]) {
        flightPresenter?.applyFilter(flightFilter: flightFilter)
        self.navigationController.popViewController(animated: true)
    }
    
}

