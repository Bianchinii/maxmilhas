//
//  AppCoordinator.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {

    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var coordinatorDelegate: CoordinatorDelegate?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        setupNavigationBar()
        showSearchFlight()
    }
    
    private func showSearchFlight() {
        let coordinator = SearchFlightsCoordinator(navigationController: self.navigationController,
                                                       delegate: nil)
        coordinator.start()
        self.childCoordinators.append(coordinator)
    }
    
    private func setupNavigationBar() {
                
        let appearance = UINavigationBar.appearance()
        appearance.tintColor = .white
        appearance.barTintColor = Colors.blueThemeColor
        appearance.titleTextAttributes = [NSAttributedStringKey.font:
            UIFont(name: "Montserrat-Bold", size: 14)!,
                                          NSAttributedStringKey.foregroundColor: UIColor.white] as [NSAttributedStringKey : Any]
    }
}
