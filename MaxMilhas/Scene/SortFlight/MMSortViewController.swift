//
//  MMSortViewController.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

protocol MMSortViewControllerDelegate {
    func didTapToApply()
}

class MMSortViewController: BaseViewController {

    var delegate: MMSortViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}
