//
//  FlightPresenter.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 20/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

protocol FlightPresenter {
    func viewDidLoad()
    func getFlightDataSurvey() -> FlightDataSurvey
    func getFlightFilterList() -> [FlightFilter]
    func applyFilter(flightFilter: [FlightFilter])
}

protocol FlightView {
    func showActivityIndicator()
    func hiddenActivityIndicator()
    func showData(list: (outbound: [FlightPresentation], inbound: [FlightPresentation]))
}

enum DirectionType {
    case outbound
    case inbound
}

struct FlightPresentation {
    var id: String
    var stops: Int
    var duration: Int
    var origin: String
    var destin: String
    var price: Float
    var departureDate: Date
    var arrivalDate: Date
    var departureTime: Time
    var arrivalTime: Time
    var direction: DirectionType
    
    init(id: String, stops: Int, duration: Int, origin: String, destin: String, price: Float, departureDate: Date, arrivalDate: Date , direction: String) {
        self.id = id
        self.stops = stops
        self.duration = duration
        self.origin = origin
        self.destin = destin
        self.price = price
        self.departureDate = departureDate
        self.arrivalDate = arrivalDate
        self.departureTime = departureDate.time
        self.arrivalTime = arrivalDate.time
        self.direction = direction == "outbound" ? .outbound : .inbound
    }
}


struct FlightDataSurvey {
    var notStops = 0
    var stops = 0
    var morningFlights = 0
    var eveningFlights = 0
    var nightFlights = 0
    var dawnFlights = 0
    
}

class FlightPresenterImplementation: FlightPresenter {
    
    var view: FlightView
    var gateway: SearchFlightsGateway
    var flightDataSurvey = FlightDataSurvey()
    var flightFilterList: [FlightFilter] = []
    
    private var outboundFlights: [FlightPresentation] = []
    private var inboundFlights: [FlightPresentation] = []
    
    init(view: FlightView, gateway: SearchFlightsGateway) {
        self.view = view
        self.gateway = gateway
    }
    
    
    //MARK: Public Functions
    func viewDidLoad() {
        
        view.showActivityIndicator()
        gateway.fetchFlights { (list, error) in
           
            if let list = list {
                if list.outbound.count > 0  || list.inbound.count > 0 {
                    self.view.hiddenActivityIndicator()
                    
                    self.outboundFlights = self.convertFlightToFlightPresentation(list: list.outbound)
                    self.inboundFlights = self.convertFlightToFlightPresentation(list: list.inbound)
                    self.mapperFlightDataSurvey()
                    self.view.showData(list: (outbound: self.outboundFlights,
                                              inbound: self.inboundFlights))
                    
                }
                
            } else {
                
              self.view.hiddenActivityIndicator()
            }
        }
    }
    
    
    func getFlightDataSurvey() -> FlightDataSurvey {
        return flightDataSurvey
    }
    
    func getFlightFilterList() -> [FlightFilter] {
        return flightFilterList
    }
    
    func applyFilter(flightFilter: [FlightFilter]) {
        flightFilterList = flightFilter
        applyFilterFlights(flightFilter: flightFilter)
    }
    
    
    //MARK: Private Functions
    internal func applyFilterFlights(flightFilter: [FlightFilter]) {
        
        let list = outboundFlights + inboundFlights
        var filterList = list
        
        if flightFilter.contains(.morning) {
            filterList = list.filter {
                $0.departureTime >= TimeFactory.departureMorning() && $0.arrivalTime <= TimeFactory.arrivalMorning()
            }
        }
        
        if flightFilter.contains(.evening) {
            filterList = list.filter {
                $0.departureTime >= TimeFactory.departureEvening() && $0.arrivalTime <= TimeFactory.arrivalEvening()
            }
        }
        
        if flightFilter.contains(.night) {
            filterList = list.filter {
                $0.departureTime >= TimeFactory.departureNight() && $0.arrivalTime <= TimeFactory.arrivalNight()
            }
        }
        
        if flightFilter.contains(.dawn) {
            filterList = list.filter {
                $0.departureTime >= TimeFactory.departureDawn() && $0.arrivalTime <= TimeFactory.arrivalDawn()
            }
        }
        
        if flightFilter.contains(.stop) {
            filterList = list.filter {
                $0.stops > 0
            }
        }
    
        if flightFilter.contains(.notStop) {
            filterList = list.filter {
                $0.stops == 0
            }
        }
        
        self.view.showData(list: (outbound: filterList.filter { $0.direction == DirectionType.outbound },
                                  inbound: filterList.filter { $0.direction == DirectionType.inbound}))
        
    }
    
    
    private func mapperFlightDataSurvey() {
        
        let flights = outboundFlights + inboundFlights
        
        let notStops = flights.filter {
            $0.stops == 0 }
            .count
        
        let stops = flights.filter {
            $0.stops >= 1
            }.count
    
        let morningFlights = flights.filter {
            $0.departureTime >= TimeFactory.departureMorning() && $0.arrivalTime <= TimeFactory.arrivalMorning()
            }.count
        
        let eveningFlights = flights.filter {
            $0.departureTime >= TimeFactory.departureEvening() && $0.arrivalTime <= TimeFactory.arrivalEvening()
            }.count
        
        let nightFlights = flights.filter {
            $0.departureTime >= TimeFactory.departureNight() && $0.arrivalTime <= TimeFactory.arrivalNight()
            }.count
        
        let dawnFlights = flights.filter {
            $0.departureTime >= TimeFactory.departureDawn() && $0.arrivalTime <= TimeFactory.arrivalDawn()
            }.count
        
        flightDataSurvey = FlightDataSurvey(notStops: notStops,
                                            stops: stops,
                                            morningFlights: morningFlights,
                                            eveningFlights: eveningFlights,
                                            nightFlights: nightFlights,
                                            dawnFlights: dawnFlights)
    }
    
    private func convertFlightToFlightPresentation(list: [MMFlight]) -> [FlightPresentation]{
        
        var presentationList: [FlightPresentation] = []
        
        for flight in list {
        
            let id = flight.id
            let stop = flight.stops
            let duration = flight.duration
            let from = flight.from
            let origin = from
            let destin = flight.to
            let price = flight.pricing?.ota?.saleTotal
            let departureDate = flight.departureDate
            let arrivalDate = flight.arrivalDate
            let direction = flight.direction
            
            addObj(list: &presentationList,
                   id: id,
                   stop: stop,
                   duration: duration,
                   origin: origin,
                   destin: destin,
                   price: price,
                   departureDate: departureDate,
                   arrivalDate: arrivalDate,
                   direction: direction)
        }
        
        return presentationList
    }
    
    private func addObj(list: inout [FlightPresentation],
                        id: String?,
                        stop: Int?,
                        duration: Int?,
                        origin: String?,
                        destin: String?,
                        price: Float?,
                        departureDate: String? ,
                        arrivalDate: String?,
                        direction:String? ) {
        
        guard let id = id else { return }
        guard let stop = stop else { return }
        guard let duration = duration else { return }
        guard let origin = origin else { return }
        guard let destin = destin else { return }
        guard let price = price else { return }
        guard let departureDate = departureDate else { return }
        guard let arrivalDate = arrivalDate else { return }
        guard let direction = direction else { return }
        
        
        if let departure = Date.converted(from: departureDate, format: Date.kDefaultDateFormat),
            let arrival = Date.converted(from: arrivalDate, format: Date.kDefaultDateFormat) {
        
            list.append(FlightPresentation(id: id,
                                           stops: stop,
                                           duration: duration,
                                           origin: origin,
                                           destin: destin,
                                           price: price, departureDate: departure,
                                           arrivalDate: arrival,
                                           direction: direction))
        }
    }
}
