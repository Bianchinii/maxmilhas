//
//  FlightConfigurator.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 20/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

protocol FlightConfigurator {
    func configure(view: MMSearchFlightsViewController)
}

class FlightConfiguratorImplementation: FlightConfigurator {
    
    func configure(view: MMSearchFlightsViewController) {

        let gateway = SearchFlightsAPI()
        let presenter = FlightPresenterImplementation(view: view, gateway: gateway)
        view.presenter = presenter
        view.coordinator?.flightPresenter = presenter
    }
}
