//
//  FlightTableViewCell.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class FlightTableViewCell: UITableViewCell {

    @IBOutlet weak var orignLabel: MMLabel!
    @IBOutlet weak var destinLabel: MMLabel!
    @IBOutlet weak var originPriceLabel: MMLabel!
    @IBOutlet weak var destiniPriceLabel: MMLabel!
    @IBOutlet weak var descriptionLabel: MMLabel!
    @IBOutlet weak var priceLabel: MMLabel!
    @IBOutlet weak var buyButton: MMButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setItem(flight: FlightPresentation) {
        
        orignLabel.text = flight.origin.uppercased()
        destinLabel.text = flight.destin.uppercased()
        originPriceLabel.text = Date.shortString(from: flight.departureDate, format: Date.kHourAndMinutes)
        destiniPriceLabel.text = Date.shortString(from: flight.arrivalDate, format: Date.kHourAndMinutes)
        
        let hours = flight.duration / 60
        let minutes =  (flight.duration % 60)
        
        descriptionLabel.text = "\(hours)h \(minutes)m, \(flight.stops) parada"
        
        priceLabel.text =  "R$ \(flight.price)"

    }
}
