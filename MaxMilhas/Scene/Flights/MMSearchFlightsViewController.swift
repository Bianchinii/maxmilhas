//
//  MMSearchFlightsViewController.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

protocol MMSearchFlightsViewControllerDelegate {
    func didTapFilter(data: FlightDataSurvey)
    func didTapSort()
}

enum ListType {
    case outbound
    case inbound
}

class MMSearchFlightsViewController: BaseViewController {

    @IBOutlet weak var flightTableView: UITableView!
    @IBOutlet weak var outboundButton: MMFlightDestinationButton!
    @IBOutlet weak var inboundButton: MMFlightDestinationButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    internal let cellIdentifier = "FlightTableViewCell"
    private var configurator = FlightConfiguratorImplementation()
    var presenter: FlightPresenter!
    var coordinator: SearchFlightsCoordinator?
    var delegate: MMSearchFlightsViewControllerDelegate?
    
    internal var outboundList: [FlightPresentation] = []
    internal var inboundList: [FlightPresentation] = []
    internal let heightCell = CGFloat(103.0)
    internal var listType: ListType = .outbound
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(view: self)
        presenter.viewDidLoad()
    }
    
    //MARK: - IBAction
    @IBAction func tapFilter(_ sender: Any) {
        delegate?.didTapFilter(data: presenter.getFlightDataSurvey())
    }
    
    @IBAction func tapSort(_ sender: Any) {
        delegate?.didTapSort()
    }
    
    @IBAction func tapOutbound(_ sender: Any) {
        listType = .outbound
        outboundButton.alpha = 1
        inboundButton.alpha = 0.5
        flightTableView.reloadData()
    }
    
    @IBAction func tapInbound(_ sender: Any) {
        listType = .inbound
        outboundButton.alpha = 0.5
        inboundButton.alpha = 1
        flightTableView.reloadData()
    }
    
    
    //MARK: - Helper
    internal func listBySelection() -> [FlightPresentation] {
        return listType == .outbound ? outboundList : inboundList
    }
}

extension MMSearchFlightsViewController: FlightView {
    
    func showActivityIndicator() {
        activityIndicator.startAnimating()
    }
    
    func hiddenActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        
    }
    
    func showData(list: (outbound: [FlightPresentation], inbound: [FlightPresentation])) {
        outboundList = list.outbound
        inboundList = list.inbound
        flightTableView.reloadData()
    }
}


extension MMSearchFlightsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listBySelection().count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier,
                                                 for: indexPath) as! FlightTableViewCell
        
        cell.setItem(flight: listBySelection()[indexPath.row])
        return cell
    }
}

extension MMSearchFlightsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) { }
}
