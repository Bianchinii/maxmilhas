//
//  FilterFlightPresenter.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit


protocol FilterFlightPresenter {
    
}

protocol FilterFlightView {
    
}

enum FlightFilter {
    case morning
    case evening
    case night
    case dawn
    case notStop
    case stop
}

class FilterFlightPresenterImplementation: FilterFlightPresenter {

    var view: FilterFlightView
    
    init(view: FilterFlightView) {
        self.view = view
    }
    
}
