//
//  FilterFlightViewController.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit


protocol FilterFlightViewControllerDelegate {
    func didTapToApplyFilter(flightFilter: [FlightFilter])
    func didTapCleanButton()
}

class FilterFlightViewController: BaseViewController {

    var delegate: FilterFlightViewControllerDelegate?
    let configurator = FilterFlightConfiguratorImplementation()
    var presenter: FilterFlightPresenterImplementation!
    var flightDataSurvey = FlightDataSurvey()
    var filterList: [FlightFilter] = []
    
    @IBOutlet weak var morningCheckButton: MMCheckButton!
    @IBOutlet weak var morningLabel: UILabel!
    @IBOutlet weak var eveningCheckButton: MMCheckButton!
    @IBOutlet weak var eveningLabel: UILabel!
    @IBOutlet weak var nightCheckButton: MMCheckButton!
    @IBOutlet weak var nightLabel: UILabel!
    @IBOutlet weak var dawnCheckButton: MMCheckButton!
    @IBOutlet weak var dawnLabel: UILabel!
    @IBOutlet weak var notStopsCheckButton: MMCheckButton!
    @IBOutlet weak var notStopsLabel: UILabel!
    @IBOutlet weak var stopsCheckButton: MMCheckButton!
    @IBOutlet weak var stopsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(view: self)
        setupUI()
    }
    

    private func setupUI() {
        addCleanButton()
        fillAccountants()
        fillFilterButtons()
    }
    
    
    private func fillAccountants() {
        morningLabel.text = "MANHÃ - 06:00 às 11:59 (\(flightDataSurvey.morningFlights) VOOS)"
        eveningLabel.text = "TARDE - 12:00 às 17:59 (\(flightDataSurvey.eveningFlights) VOOS)"
        nightLabel.text = "NOITE - 18:00 às 23:59 (\(flightDataSurvey.nightFlights) VOOS)"
        dawnLabel.text = "MADRUGADA - 00:00 às 05:59 (\(flightDataSurvey.dawnFlights) VOOS)"
        notStopsLabel.text = "VOO DIRETO (\(flightDataSurvey.notStops) VOOS)"
        stopsLabel.text = "PARADA (\(flightDataSurvey.stops) VOOS)"
    }
    
    
    private func fillFilterButtons() {
        morningCheckButton.isSelected = filterList.contains(.morning)
        eveningCheckButton.isSelected = filterList.contains(.evening)
        nightCheckButton.isSelected = filterList.contains(.night)
        dawnCheckButton.isSelected = filterList.contains(.dawn)
        notStopsCheckButton.isSelected = filterList.contains(.notStop)
        stopsCheckButton.isSelected = filterList.contains(.stop)
        
        
        let list = [morningCheckButton,
                    eveningCheckButton,
                    nightCheckButton,
                    dawnCheckButton,
                    notStopsCheckButton,
                    stopsCheckButton]
        
        for button in list {
            button?.setBackgroundImage((button?.isSelected)! ? #imageLiteral(resourceName: "check_button_on") : #imageLiteral(resourceName: "check_button_off"), for: .normal)
        }
    }
    
    private func addCleanButton() {
        let btn = UIButton(type: .custom)
        btn.setTitle("LIMPAR", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Montserrat-Bold", size: 14)
        btn.frame = CGRect(x: 0, y: 0, width: 61, height: 18)
        btn.addTarget(self, action: #selector(FilterFlightViewController.touchCleanButton), for: .touchUpInside)
        let item = UIBarButtonItem(customView: btn)
    
        self.navigationItem.setRightBarButtonItems([item], animated: true)
    }
    
    
    @IBAction func touchMorning(_ sender: Any) {
        if filterList.contains(.morning) {
            if let index = filterList.index(where: { $0 == FlightFilter.morning }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.morning)
        }
    }
    
    
    @IBAction func touchEvening(_ sender: Any) {
        if filterList.contains(.evening) {
            if let index = filterList.index(where: { $0 == FlightFilter.evening }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.evening)
        }
    }
    
    
    @IBAction func touchNight(_ sender: Any) {
        if filterList.contains(.night) {
            if let index = filterList.index(where: { $0 == FlightFilter.night }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.night)
        }
    }
    
    
    @IBAction func touchDawn(_ sender: Any) {
        if filterList.contains(.dawn) {
            if let index = filterList.index(where: { $0 == FlightFilter.dawn }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.dawn)
        }
    }
    
    
    @IBAction func touchNotStops(_ sender: Any) {
        if filterList.contains(.notStop) {
            if let index = filterList.index(where: { $0 == FlightFilter.notStop }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.notStop)
        }
    }
    
    
    @IBAction func touchStops(_ sender: Any) {
        if filterList.contains(.stop) {
            if let index = filterList.index(where: { $0 == FlightFilter.stop }) {
                filterList.remove(at: index)
            }
        } else {
            filterList.append(.stop)
        }
    }
    
    
    @objc func touchCleanButton(sender: Any) {
        delegate?.didTapCleanButton()
    }
    
    @IBAction func touchApplyFilter(_ sender: Any) {
        delegate?.didTapToApplyFilter(flightFilter: filterList)
    }
}

extension FilterFlightViewController : FilterFlightView {
    
}
