//
//  FilterFlightConfigurator.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

protocol FilterFlightConfigurator {
    func configure(view: FilterFlightViewController)
}

class FilterFlightConfiguratorImplementation: FilterFlightConfigurator {

    func configure(view: FilterFlightViewController) {
        view.presenter = FilterFlightPresenterImplementation(view: view)
    }
}
