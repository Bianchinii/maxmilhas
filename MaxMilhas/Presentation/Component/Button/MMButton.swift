//
//  MMButton.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class MMButton: UIButton {

    @IBInspectable var borderColor: UIColor? {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
}
