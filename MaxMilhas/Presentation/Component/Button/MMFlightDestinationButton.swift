//
//  MMFlightDestinationButton.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

@IBDesignable
class MMFlightDestinationButton: MMButton {

    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                borderColor = Colors.greenThemeColor
            } else {
                borderColor = Colors.greenThemeColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    func commonInit() {
        let border = CALayer()
        border.backgroundColor = Colors.greenThemeColor.cgColor
        border.frame = CGRect(x:0,
                              y: 0,
                              width:1,
                              height:1)
        
        self.layer.addSublayer(border)
        
    }
}
