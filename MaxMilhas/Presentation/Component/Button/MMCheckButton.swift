//
//  MMCheckButton.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit


@IBDesignable
class MMCheckButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.commonInit()
    }
    
    func commonInit() {
        self.addTarget(self, action: #selector(handleCheckboxTap), for: .touchUpInside)
    }
    
    @objc func handleCheckboxTap(sender: Any) {
        
        if self.isSelected {
            self.setBackgroundImage(#imageLiteral(resourceName: "check_button_off"), for: .normal)
            self.isSelected = false
        } else {
            self.setBackgroundImage(#imageLiteral(resourceName: "check_button_on"), for: .normal)
            self.isSelected = true
        }
    }
}
