//
//  MMLabel.swift
//  MaxMilhas
//
//  Created by Ítalo Bianchini on 19/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit

class MMLabel: UILabel {

    @IBInspectable var labelLineSpacing: CGFloat = 0 {
        didSet {
            self.updateLineSpacing()
        }
    }
    
    override var text: String? {
        didSet {
            self.updateLineSpacing()
        }
    }
    
    override var textColor: UIColor! {
        didSet {
            self.updateLineSpacing()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.height += 5
        
        return size
    }
    
    func updateLineSpacing() {
        
        guard let text = self.text,
            let font = self.font,
            let textColor = self.textColor else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = self.labelLineSpacing
        paragraphStyle.alignment = self.textAlignment
        self.attributedText = NSAttributedString(string: text, attributes: [NSAttributedStringKey.font: font,
                                                                            NSAttributedStringKey.foregroundColor: textColor,
                                                                            NSAttributedStringKey.paragraphStyle: paragraphStyle])
    }

}
