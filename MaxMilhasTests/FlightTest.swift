//
//  FlightTest.swift
//  MaxMilhasTests
//
//  Created by Ítalo Bianchini Werneck Fragoso on 21/02/19.
//  Copyright © 2019 Ítalo Bianchini. All rights reserved.
//

import UIKit
import XCTest
import Foundation

@testable import MaxMilhas

class FlightTest: MaxMilhasTests {
    
    private lazy var controller: MMSearchFlightsViewController = {
        let controller = MMSearchFlightsViewController.initFromStoryboard(named: "Flights")
        _ = controller.view
        return controller
    }()
    
    func testGetFlights(){
        
        let gateway = SearchFlightsAPI()
        let presenter = FlightPresenterImplementation(view: controller, gateway: gateway)
        let expectation = self.expectation(description: "Scaling")
        var passed = false
        
        presenter.gateway.fetchFlights { (list, error) in
            if let list = list {
                passed = list.outbound.count > 0 || list.inbound.count > 0
            }
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(passed)
    }
}
